#ifndef __program__
#define __program__

#include <string>
#include <fstream>
using namespace std;

namespace simple_shapes
{
	struct aphorism {
		char author[256];
	};
	void In(aphorism &a, ifstream &ifst);
	void Out(aphorism &a, ofstream &ofst);


	struct proverb {
		char country[256];
	};
	void In(proverb &p, ifstream &ifst);
	void Out(proverb &p, ofstream &ofst);
	
	struct riddle {
		char answer[256];
	};
	void In(riddle &r, ifstream &ifst);
	void Out(riddle &r, ofstream &ofst);
	
	// ���������, ���������� ��� ��������� ������
	struct shape
	{
		char text[256];
		int assessment;
		
		// �������� ������ ��� ������ �� �����
		enum key { APHORISM, PROVERB, RIDDLE};
		key k; // ����
			   // ������������ ������������
		union 
		{ // ���������� ���������
			aphorism a;
			proverb p;
			riddle r;
		};
	};
	
	int Count(shape &s);
	shape* In(ifstream &ifst);
	void Out(shape &s, ofstream &ofst);

	struct  Node
	{
		shape* x;
		Node *Next;
		Node *Prev;
	};

	struct List
	{
		Node *Head, *Tail; //������ ������� � ��� ��� ���������
		int size; //����� ��������� � ������
	};

	void Init(struct List &lst);
	void Clear(struct List &lst);
	void In(struct List &lst, ifstream &ifst);
	void Out(struct List &lst, ofstream &ofst);
	bool Compare(shape *first, shape *second);
	void Sort(struct List &lst);
	void OutAphorisms(struct list &lst, ofstream &ofst);
} // end simple_shapes namespace
#endif
